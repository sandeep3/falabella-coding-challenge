<?php namespace falabella;

class Index
{
    public function Output()
    {

        for ($i = 1; $i <= 100; $i++) {
            $calculate = new Calculate();
            $result = new FindResult();
            $result->number = $i;
            $result->remainder3 = $calculate->calculateRemainder($i, 3);
            $result->remainder5 = $calculate->calculateRemainder($i, 5);
            $result->remainder35 = $calculate->addRemainder($result->remainder3, $result->remainder5);
            $write = new Result();
            $write->writeAnswer($result);

        }
    }

}
// $res = new Index();
// $res->Output();
// return $res;
