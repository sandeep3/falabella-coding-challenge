<?php
namespace falabella;

class FindResult
{
    public $remainder3;
    public $remainder5;
    public $remainder35;
    public $number;
}
