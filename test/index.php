<?php namespace falabella;

use PHPUnit\Framework\TestCase;

// use FindResult;
// use WriteResult;

class WriterTest extends TestCase
{
    private $_writer;
    private $_result_test;
    private $_outPut;

    public function inIt($remainder35, $remainder5, $remainder3, $number)
    {

        $this->_writer = new \stdClass();
        $this->_result_test = new \stdClass();
        $this->_result_test = new FindResult();
        $this->_writer = new Result();
        $this->_result_test->remainder35 = $remainder35;
        $this->_result_test->remainder5 = $remainder5;
        $this->_result_test->remainder3 = $remainder3;
        $this->_result_test->number = $number;
    }

    public function test35()
    {
        $this->inIt(0, 0, 0, 15);
        $this->expectOutputString("Linianos\n");
        $this->_writer->writeAnswer($this->_result_test);

    }

    public function test5()
    {
        $this->inIt(1, 0, 1, 50);
        $this->expectOutputString("IT\n");
        $this->_writer->writeAnswer($this->_result_test);

    }

    public function test3()
    {
        $this->inIt(1, 1, 0, 9);
        $this->expectOutputString("Linio\n");
        $this->_writer->writeAnswer($this->_result_test);
    }
    public function testOutput()
    {
        $this->_outPut = new index();
        return $this->_outPut->Output();
    }
}
