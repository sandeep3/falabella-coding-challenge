<?php
namespace falabella;

use PHPUnit\Framework\TestCase;

class CalcFormAddTest extends TestCase
{

    private $_calculate;

    public function setUp()
    {

        $this->_calculate = new Calculate();
    }

    public function testThree()
    {

        $this->assertEquals(0, $this->_calculate->calculateRemainder(3, 3));
        $this->assertEquals(0, $this->_calculate->calculateRemainder(12, 3));
        $this->assertEquals(0, $this->_calculate->calculateRemainder(27, 3));
    }

    public function testFive()
    {

        $this->assertEquals(0, $this->_calculate->calculateRemainder(5, 5));
        $this->assertEquals(0, $this->_calculate->calculateRemainder(50, 5));
        $this->assertEquals(0, $this->_calculate->calculateRemainder(50, 5));
    }

    public function testThreeAndFive()
    {

        $this->assertEquals(0, $this->_calculate->addRemainder(0, 0));
        $this->assertEquals(0, $this->_calculate->addRemainder(($this->_calculate->calculateRemainder(15, 5)), ($this->_calculate->calculateRemainder(15, 3))));

    }

    public function testNone()
    {

        $this->assertNotEquals(0, $this->_calculate->calculateRemainder(4, 3));
        $this->assertNotEquals(0, $this->_calculate->calculateRemainder(7, 5));
        $this->assertNotEquals(0, $this->_calculate->addRemainder(1, 0));
        $this->assertNotEquals(0, $this->_calculate->addRemainder(($this->_calculate->calculateRemainder(17, 5)), ($this->_calculate->calculateRemainder(17, 3))));
    }
}
